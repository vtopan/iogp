"""
iogp.data: Various data processing tools.

Author: Vlad Topan (vtopan/gmail)
"""
from .fileid import get_file_type, get_ft_category, get_ft_ext, FT_UNKNOWN, FT_EMPTY, FT_CATEGORY
from .carve import extract_embedded_files, find_encoded_data
from .ds import AttrDict, Config, dict_to_AttrDict
from .archive import Archive
from .str import (ellipsis, parse_num, parse_percent, format_date, format_datetime, format_num,
        format_percent, format_size, int_to_bytes, bytes_to_int, decode_cp437, clean_filename,
        UNICODE_TO_CP437, CP437_TO_UNICODE)
from .dataview import DataView
